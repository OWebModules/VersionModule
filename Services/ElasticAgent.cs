﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VersionModule.Models;

namespace VersionModule.Services
{
    public class ElasticAgent
    {
        private Globals globals;

        public async Task<ResultItem<clsOntologyItem>> GetOItem(string idOItem, string type = null)
        {
            var taskResult = await Task.Run<ResultItem<clsOntologyItem>>(() =>
            {
                var result = new ResultItem<clsOntologyItem>
                {
                    ResultState = globals.LState_Success.Clone()
                };

                var searchItem = new List<clsOntologyItem>
                {
                    new clsOntologyItem
                    {
                        GUID = idOItem
                    }
                };

                var dbReaderObject = new OntologyModDBConnector(globals);

                if (type == null || type == globals.Type_Object)
                {
                    result.ResultState = dbReaderObject.GetDataObjects(searchItem);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while reading Object";
                        return result;
                    }

                    result.Result = dbReaderObject.Objects1.FirstOrDefault();
                    if (result.Result != null)
                    {
                        return result;
                    }

                    if (type == globals.Type_Object)
                    {
                        result.ResultState = globals.LState_Error.Clone();
                        result.ResultState.Additional1 = "The object cannot be found!";
                        return result;
                    }
                }

                if (type == null || type == globals.Type_Class)
                {
                    result.ResultState = dbReaderObject.GetDataClasses(searchItem);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while reading Class";
                        return result;
                    }

                    result.Result = dbReaderObject.Classes1.FirstOrDefault();
                    if (result.Result != null)
                    {
                        return result;
                    }

                    if (type == globals.Type_Class)
                    {
                        result.ResultState = globals.LState_Error.Clone();
                        result.ResultState.Additional1 = "The class cannot be found!";
                        return result;
                    }
                }

                if (type == null || type == globals.Type_RelationType)
                {
                    result.ResultState = dbReaderObject.GetDataRelationTypes(searchItem);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while reading RelationType";
                        return result;
                    }

                    result.Result = dbReaderObject.RelationTypes.FirstOrDefault();
                    if (result.Result != null)
                    {
                        return result;
                    }

                    if (type == globals.Type_RelationType)
                    {
                        result.ResultState = globals.LState_Error.Clone();
                        result.ResultState.Additional1 = "The relationtype cannot be found!";
                        return result;
                    }
                }

                result.ResultState = dbReaderObject.GetDataAttributeType(searchItem);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while reading AttributeType";
                    return result;
                }

                result.Result = dbReaderObject.AttributeTypes.FirstOrDefault();

                if (result.Result == null)
                {
                    result.ResultState = globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "The Id is not found in the Onotlogy-Graph!";
                    return result;
                }

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<long>> GetNextOrderId(AddVersionsRequest request)
        {
            var taskResult = await Task.Run<ResultItem<long>>(async() =>
            {
                var result = new ResultItem<long>
                {
                    ResultState = globals.LState_Success.Clone()
                };

                var isValidResult = await IsRequestValid(request);

                result.ResultState = isValidResult;

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                var resultGetRelationType = await GetOItem(request.IdRelationType, globals.Type_RelationType);
                result.ResultState = resultGetRelationType.ResultState.Clone();

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while reading RelationType";
                    return result;
                }

                var resultGetOItem = await GetOItem(request.IdRefItem);

                result.ResultState = resultGetOItem.ResultState;
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                clsOntologyItem leftItem = null;
                clsOntologyItem rightItem = null;
                clsOntologyItem relationType = null;

                if (request.Direction.GUID == globals.Direction_LeftRight.GUID)
                {
                    leftItem = resultGetOItem.Result;
                    rightItem = new clsOntologyItem
                    {
                        GUID_Parent = Config.LocalData.Class_Development_Version.GUID
                    };
                    relationType = resultGetRelationType.Result;
                }
                else
                {
                    rightItem = resultGetOItem.Result;
                    leftItem = new clsOntologyItem
                    {
                        GUID_Parent = Config.LocalData.Class_Development_Version.GUID
                    };
                    relationType = resultGetRelationType.Result;
                }

                var dbReaderNextOrderId = new OntologyModDBConnector(globals);

                result.Result = dbReaderNextOrderId.GetDataRelOrderId(leftItem, rightItem, relationType, false) + 1;

                return result;
            });
            return taskResult;
        }
        
        public async Task<clsOntologyItem> IsRequestValid(GetRefVersionsRequest request)
        {
            var taskResult = await Task.Run<clsOntologyItem>(async() =>
            {
                var result = globals.LState_Success.Clone();
                if (string.IsNullOrEmpty(request.IdRefItem))
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "The Id of the reference is not valid! It's empty!";
                    return result;
                }

                if (!globals.is_GUID(request.IdRefItem))
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "The Id of the reference is no valid GUID!";
                    return result;
                }

                if (request.Direction.GUID != globals.Direction_LeftRight.GUID && request.Direction.GUID != globals.Direction_RightLeft.GUID)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = "The Direction is not valid!";
                    return result;
                }

                var resultGetRelationType = await GetOItem(request.IdRelationType, globals.Type_RelationType);
                result = resultGetRelationType.ResultState.Clone();

                if (result.GUID == globals.LState_Error.GUID)
                {
                    result.Additional1 = "Error while reading RelationType";
                    return result;
                }

                var resultGetOItem = await GetOItem(request.IdRefItem);

                result = resultGetOItem.ResultState;
                if (result.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                

                return result;
            });

            return taskResult;
            
        }

        public async Task<ResultItem<GetModelResult>> GetModel(GetRefVersionsRequest request)
        {
            var taskResult = await Task.Run<ResultItem<GetModelResult>>(async () =>
            {
                var result = new ResultItem<GetModelResult>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new GetModelResult()
                };

                var validationResult = await IsRequestValid(request);

                result.ResultState = validationResult;

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                result.Result.Direction = request.Direction;

                var resultGetRelationType = await GetOItem(request.IdRelationType, globals.Type_RelationType);
                result.ResultState = resultGetRelationType.ResultState.Clone();

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while reading RelationType";
                    return result;
                }

                result.Result.RelationType = resultGetRelationType.Result;

                var resultGetOItem = await GetOItem(request.IdRefItem);

                result.ResultState = resultGetOItem.ResultState;
                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                result.Result.RefItem = resultGetOItem.Result;

                if (result.Result.RefItem.GUID_Parent != Config.LocalData.Class_Development_Version.GUID)
                {
                    var searchRefItemToVersions = new List<clsObjectRel>();

                    if (request.Direction.GUID == globals.Direction_LeftRight.GUID)
                    {
                        searchRefItemToVersions.Add(new clsObjectRel
                        {
                            ID_Parent_Other = Config.LocalData.Class_Development_Version.GUID,
                            ID_RelationType = result.Result.RelationType.GUID,
                            ID_Object = result.Result.RefItem.GUID
                        });
                    }
                    else
                    {
                        searchRefItemToVersions.Add(new clsObjectRel
                        {
                            ID_Parent_Object = Config.LocalData.Class_Development_Version.GUID,
                            ID_RelationType = result.Result.RelationType.GUID,
                            ID_Other = result.Result.RefItem.GUID
                        });
                    }

                    var dbReaderRefToVersions = new OntologyModDBConnector(globals);

                    result.ResultState = dbReaderRefToVersions.GetDataObjectRel(searchRefItemToVersions);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Erro while getting References to Versions";
                        return result;
                    }

                    if (result.Result.Direction.GUID == globals.Direction_LeftRight.GUID)
                    {
                        result.Result.VersionItems = dbReaderRefToVersions.ObjectRels.Select(rel => new clsOntologyItem
                        {
                            GUID = rel.ID_Other,
                            Name = rel.Name_Other,
                            GUID_Parent = rel.ID_Parent_Other,
                            Type = globals.Type_Object,
                            Val_Long = rel.OrderID
                        }).ToList();

                    }
                    else
                    {
                        result.Result.VersionItems = dbReaderRefToVersions.ObjectRels.Select(rel => new clsOntologyItem
                        {
                            GUID = rel.ID_Object,
                            Name = rel.Name_Object,
                            GUID_Parent = rel.ID_Parent_Object,
                            Type = globals.Type_Object,
                            Val_Long = rel.OrderID
                        }).ToList();
                    }

                }
                else
                {
                    result.Result.RefItem.Val_Long = 1;
                    result.Result.VersionItems.Add(result.Result.RefItem);
                }

                var searchMajor = result.Result.VersionItems.Select(version => new clsObjectAtt
                {
                    ID_Object = version.GUID,
                    ID_AttributeType = Config.LocalData.ClassAtt_Development_Version_Major.ID_AttributeType
                }).ToList();

                if (searchMajor.Any())
                {
                    var dbReaderMajor = new OntologyModDBConnector(globals);
                    result.ResultState = dbReaderMajor.GetDataObjectAtt(searchMajor);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while reading Major";
                        return result;
                    }

                    result.Result.VersionToMajor = dbReaderMajor.ObjAtts;
                }

                var searchMinor = result.Result.VersionItems.Select(version => new clsObjectAtt
                {
                    ID_Object = version.GUID,
                    ID_AttributeType = Config.LocalData.ClassAtt_Development_Version_Minor.ID_AttributeType
                }).ToList();

                if (searchMinor.Any())
                {
                    var dbReaderMinor = new OntologyModDBConnector(globals);
                    result.ResultState = dbReaderMinor.GetDataObjectAtt(searchMinor);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while reading Minor";
                        return result;
                    }

                    result.Result.VersionToMinor = dbReaderMinor.ObjAtts;
                }

                var searchBuild = result.Result.VersionItems.Select(version => new clsObjectAtt
                {
                    ID_Object = version.GUID,
                    ID_AttributeType = Config.LocalData.ClassAtt_Development_Version_Build.ID_AttributeType
                }).ToList();

                if (searchBuild.Any())
                {
                    var dbReaderBuild = new OntologyModDBConnector(globals);
                    result.ResultState = dbReaderBuild.GetDataObjectAtt(searchBuild);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while reading Build";
                        return result;
                    }

                    result.Result.VersionToBuild = dbReaderBuild.ObjAtts;
                }

                var searchRevision = result.Result.VersionItems.Select(version => new clsObjectAtt
                {
                    ID_Object = version.GUID,
                    ID_AttributeType = Config.LocalData.ClassAtt_Development_Version_Revision.ID_AttributeType
                }).ToList();

                if (searchRevision.Any())
                {
                    var dbReaderRevision = new OntologyModDBConnector(globals);
                    result.ResultState = dbReaderRevision.GetDataObjectAtt(searchRevision);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while reading Revision";
                        return result;
                    }

                    result.Result.VersionToRevision = dbReaderRevision.ObjAtts;
                }

                return result;
            });

            return taskResult;
        }

        public ElasticAgent(Globals globals)
        {
            this.globals = globals;
        }
    }
}
