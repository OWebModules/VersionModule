﻿using LogModule;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VersionModule.Models;
using VersionModule.Services;

namespace VersionModule
{
    public class VersionController : AppController
    {
        public async Task<ResultItem<VersionItem>> AddVersion(AddVersionsRequest request)
        {
            var taskResult = await Task.Run<ResultItem<VersionItem>>(async() =>
            {
                var result = new ResultItem<VersionItem>
                {
                    ResultState = Globals.LState_Success.Clone(),
                    Result = new VersionItem()
                };

                if (!request.IsValid)
                {
                    result.ResultState = Globals.LState_Error.Clone();
                    result.ResultState.Additional1 = request.ValidationViolationMessage;
                    return result;
                }
                var serviceAgent = new ElasticAgent(Globals);
                var relationConfig = new clsRelationConfig(Globals);
                var transaction = new clsTransaction(Globals);

                var getVersionsResult = await GetRefVersions(request);

                result.ResultState = getVersionsResult.ResultState;
                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    return result;
                }
                var versionItem = new clsOntologyItem
                {
                    GUID = Globals.NewGUID,
                    GUID_Parent = Config.LocalData.Class_Development_Version.GUID,
                    Type = Globals.Type_Object,
                    Val_Long = 1
                };

                clsObjectAtt major = null;
                clsObjectAtt minor = null;
                clsObjectAtt build = null;
                clsObjectAtt revision = null;

                clsObjectRel refToVersion = null;

                long orderId = 1;

                var lastVersion = getVersionsResult.Result.DevelopmentVersionList.OrderByDescending(version => version.OrderId).OrderByDescending(version => version.Major.Value).
                        ThenByDescending(version => version.Minor.Value).
                        ThenByDescending(version => version.Build.Value).
                        ThenByDescending(version => version.Revision.Value).FirstOrDefault();
                if (lastVersion != null)
                {
                    orderId = lastVersion.OrderId + 1;
                }

                if (request.AddVersionType == AddVersionType.Set)
                {
                    

                 
                    
                    major = relationConfig.Rel_ObjectAttribute(versionItem, Config.LocalData.AttributeType_Major, request.Major.Value);
                    minor = relationConfig.Rel_ObjectAttribute(versionItem, Config.LocalData.AttributeType_Minor, request.Minor.Value);
                    build = relationConfig.Rel_ObjectAttribute(versionItem, Config.LocalData.AttributeType_Build, request.Build.Value);
                    revision = relationConfig.Rel_ObjectAttribute(versionItem, Config.LocalData.AttributeType_Revision, request.Revision.Value);

                    
                }
                else
                {
                    if (lastVersion != null)
                    {
                        major = relationConfig.Rel_ObjectAttribute(versionItem, Config.LocalData.AttributeType_Major, lastVersion.Major.Value + (request.AddVersionType == AddVersionType.IncreaseMajor ? request.Increase.Value : 0));
                        minor = relationConfig.Rel_ObjectAttribute(versionItem, Config.LocalData.AttributeType_Minor, lastVersion.Minor.Value + (request.AddVersionType == AddVersionType.IncreaseMinor ? request.Increase.Value : 0));
                        build = relationConfig.Rel_ObjectAttribute(versionItem, Config.LocalData.AttributeType_Build, lastVersion.Build.Value + (request.AddVersionType == AddVersionType.IncreaseBuild ? request.Increase.Value : 0));
                        revision = relationConfig.Rel_ObjectAttribute(versionItem, Config.LocalData.AttributeType_Revision, lastVersion.Revision.Value + (request.AddVersionType == AddVersionType.IncreaseRevision ? request.Increase.Value : 0));
                    }
                    else
                    {
                        major = relationConfig.Rel_ObjectAttribute(versionItem, Config.LocalData.AttributeType_Major, 0);
                        minor = relationConfig.Rel_ObjectAttribute(versionItem, Config.LocalData.AttributeType_Minor, 0);
                        build = relationConfig.Rel_ObjectAttribute(versionItem, Config.LocalData.AttributeType_Build, 0);
                        revision = relationConfig.Rel_ObjectAttribute(versionItem, Config.LocalData.AttributeType_Revision, 1);
                    }
                    
                }

                if (request.Direction.GUID == Globals.Direction_LeftRight.GUID)
                {
                    refToVersion = relationConfig.Rel_ObjectRelation(getVersionsResult.Result.RefItem, versionItem, getVersionsResult.Result.RelationType, orderId: orderId);
                }
                else
                {
                    refToVersion = relationConfig.Rel_ObjectRelation(versionItem, getVersionsResult.Result.RefItem, getVersionsResult.Result.RelationType, orderId: orderId);
                }

                versionItem.Name = $"{major.Val_Lng}.{minor.Val_Lng}.{build.Val_Lng}.{revision.Val_Lng}";
                result.ResultState = transaction.do_Transaction(versionItem);

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while saveing VersionItem";
                    return result;
                }

                result.ResultState = transaction.do_Transaction(refToVersion);

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while saveing Reference to VersionItem";
                    transaction.rollback();
                    return result;
                }

                result.ResultState = transaction.do_Transaction(major);
                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while saveing major";
                    transaction.rollback();
                    return result;
                }

                result.ResultState = transaction.do_Transaction(minor);
                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while saveing minor";
                    transaction.rollback();
                    return result;
                }

                result.ResultState = transaction.do_Transaction(build);
                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while saveing build";
                    transaction.rollback();
                    return result;
                }

                result.ResultState = transaction.do_Transaction(revision);
                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while saveing revision";
                    transaction.rollback();
                    return result;
                }

                var logController = new LogController(Globals);

                var logResult = await logController.CreateLogEntry(request.LogState,
                    versionItem,
                    request.UserItem,
                    request.Message,
                    request.LogDate);

                result.ResultState = logResult.ResultState;
                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while saving Logentry";
                    return result;
                }


                var logEntryRelation = logController.RelLogEntryToRef(logResult.Result, getVersionsResult.Result.RefItem, relationConfig);
                result.ResultState = transaction.do_Transaction(logEntryRelation);

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while relating Logentry to reference!";
                    return result;
                }


                return result;
            });

            return taskResult;
        }

        


        public async Task<ResultItem<GetRefVersionsResult>> GetRefVersions(GetRefVersionsRequest request)
        {
            var taskResult = await Task.Run<ResultItem<GetRefVersionsResult>>(async() =>
            {
                var result = new ResultItem<GetRefVersionsResult>
                {
                    ResultState = Globals.LState_Success.Clone(),
                    Result = new GetRefVersionsResult()
                };

                var elasticAgent = new ElasticAgent(Globals);
                var getModelResult = await elasticAgent.GetModel(request);

                result.ResultState = getModelResult.ResultState;

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    return result;
                }

                result.Result.RefItem = getModelResult.Result.RefItem;
                result.Result.Direction = request.Direction;
                result.Result.RelationType = getModelResult.Result.RelationType;
                result.Result.DevelopmentVersionList = (from versionItem in getModelResult.Result.VersionItems
                                                        join versionMajor in getModelResult.Result.VersionToMajor on versionItem.GUID equals versionMajor.ID_Object
                                                        join versionMinor in getModelResult.Result.VersionToMinor on versionItem.GUID equals versionMinor.ID_Object
                                                        join versionBuild in getModelResult.Result.VersionToBuild on versionItem.GUID equals versionBuild.ID_Object
                                                        join versionRevision in getModelResult.Result.VersionToRevision on versionItem.GUID equals versionRevision.ID_Object
                                                        select new VersionItem
                                                        {
                                                            RefItem = getModelResult.Result.RefItem,
                                                            Id = versionItem.GUID,
                                                            Name = versionItem.Name,
                                                            Direction = getModelResult.Result.Direction,
                                                            RelationType = getModelResult.Result.RelationType,
                                                            Major = new LongAttribute
                                                            {
                                                                IdAttribute = versionMajor.ID_Attribute,
                                                                Value = versionMajor.Val_Lng.Value
                                                            },
                                                            Minor = new LongAttribute
                                                            {
                                                                IdAttribute = versionMinor.ID_Attribute,
                                                                Value = versionMinor.Val_Lng.Value
                                                            },
                                                            Build = new LongAttribute
                                                            {
                                                                IdAttribute = versionBuild.ID_Attribute,
                                                                Value = versionBuild.Val_Lng.Value
                                                            },
                                                            Revision = new LongAttribute
                                                            {
                                                                IdAttribute = versionRevision.ID_Attribute,
                                                                Value = versionRevision.Val_Lng.Value
                                                            },
                                                            OrderId = versionItem.Val_Long.Value
                                                        }).ToList();

                return result;
            });

            return taskResult;
        }

        public VersionController(Globals globals) : base(globals)
        {
        }
    }
}
