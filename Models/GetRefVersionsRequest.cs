﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VersionModule.Models
{
    public class GetRefVersionsRequest
    {
        public string IdRefItem { get; private set; }
        public clsOntologyItem Direction { get; private set; }
        public string IdRelationType { get; private set; }

        public GetRefVersionsRequest(string idRefItem, Globals globals, clsOntologyItem direction = null, string idRelationType = null)
        {
            IdRefItem = idRefItem;
            Direction = direction;

            if (Direction == null)
            {
                Direction = globals.Direction_RightLeft;
            }

            IdRelationType = idRelationType;

            if (string.IsNullOrEmpty(IdRelationType) )
            {
                IdRelationType = Config.LocalData.RelationType_state_of.GUID;
            }
        }
    }
}
