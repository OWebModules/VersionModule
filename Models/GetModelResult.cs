﻿using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VersionModule.Models
{
    public class GetModelResult
    {
        public clsOntologyItem RefItem { get; set; }
        public clsOntologyItem Direction { get; set; }
        public clsOntologyItem RelationType { get; set; }
        public List<clsOntologyItem> VersionItems { get; set; } = new List<clsOntologyItem>();
        public List<clsObjectAtt> VersionToMajor { get; set; } = new List<clsObjectAtt>();
        public List<clsObjectAtt> VersionToMinor { get; set; } = new List<clsObjectAtt>();
        public List<clsObjectAtt> VersionToBuild { get; set; } = new List<clsObjectAtt>();
        public List<clsObjectAtt> VersionToRevision { get; set; } = new List<clsObjectAtt>();
    }
}
