﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VersionModule.Models
{
    public enum AddVersionType
    {
        IncreaseRevision = 0,
        IncreaseBuild = 1,
        IncreaseMinor = 2,
        IncreaseMajor = 3,
        Set = 4
    }

    public enum LogStateType
    {
        Information = 0,
        Create = 1,
        Obsolete = 2,
        Request = 3,
        VersionChanged = 4
    }
    public class AddVersionsRequest : GetRefVersionsRequest
    {
        public AddVersionType AddVersionType { get; private set; }

        public long? Increase { get; set; }
        public long? Major { get; set; }
        public long? Minor { get; set; }
        public long? Build { get; set; }
        public long? Revision { get; set; }

        public string Message { get; set; }

        public DateTime LogDate { get; private set; }

        public clsOntologyItem LogState { get; private set; }

        public clsOntologyItem UserItem { get; private set; }

        public string ValidationViolationMessage { get; private set; }
        public bool IsValid
        {
            get
            {
                if (AddVersionType == AddVersionType.IncreaseMajor ||
                    AddVersionType == AddVersionType.IncreaseMinor ||
                    AddVersionType == AddVersionType.IncreaseBuild ||
                    AddVersionType == AddVersionType.IncreaseRevision)
                {
                    if (Increase == null)
                    {
                        ValidationViolationMessage = "Incerease is not set!";
                        return false;
                    }
                }
                else
                {
                    if (Major == null || Minor == null || Build == null || Revision == null)
                    {
                        ValidationViolationMessage = "You must set Major, Minor, Build and Revision!";
                        return false;
                    }
                }

                return true;
            }

        }

        public AddVersionsRequest(string idRefItem,
            Globals globals,
            AddVersionType addType,
            LogStateType logState,
            clsOntologyItem userItem,
            DateTime? logDate = null,
            clsOntologyItem direction = null,
            string idRelationType = null,
            string message = null) : base(idRefItem, globals, direction, idRelationType)
        {
            clsOntologyItem oLogState = null;

            switch(logState)
            {
                case LogStateType.Create:
                    oLogState = Config.LocalData.Object_Create;
                    break;
                case LogStateType.Obsolete:
                    oLogState = Config.LocalData.Object_Obsolete;
                    break;
                case LogStateType.Request:
                    oLogState = Config.LocalData.Object_Request;
                    break;
                case LogStateType.VersionChanged:
                    oLogState = Config.LocalData.Object_Version_Changed;
                    break;
                default:
                    oLogState = Config.LocalData.Object_Information;
                    break;
            }

            LogState = oLogState;
            AddVersionType = addType;

            if (logDate == null)
            {
                LogDate = DateTime.Now;
            }
            else
            {
                LogDate = logDate.Value;
            }

            Message = message;
            if (string.IsNullOrEmpty(Message))
            {
                Message = "Version-Change";
            }

            UserItem = userItem;
        }

        public AddVersionsRequest(string idRefItem, 
        Globals globals, 
        AddVersionType addType,
        clsOntologyItem userItem,
        DateTime? logDate = null,
        clsOntologyItem logState = null,
        clsOntologyItem direction = null, 
        string idRelationType = null, 
        string message = null) : base(idRefItem, globals, direction, idRelationType)
        {
            AddVersionType = addType;

            if (logDate == null)
            {
                LogDate = DateTime.Now;
            }
            else
            {
                LogDate = logDate.Value;
            }

            if (logState == null)
            {
                LogState = Config.LocalData.Object_Information;
            }
            else
            {
                LogState = logState;
            }

            Message = message;
            if (string.IsNullOrEmpty(Message))
            {
                Message = "Version-Change";
            }

            UserItem = userItem;
        }
    }
    
}
