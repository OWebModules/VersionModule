﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VersionModule.Models
{
    public class GetRefVersionsResult
    {
        public clsOntologyItem RefItem { get; set; }
        public clsOntologyItem RelationType { get; set; }
        public clsOntologyItem Direction { get; set; }
        public List<VersionItem> DevelopmentVersionList { get; set; }
    }
}
