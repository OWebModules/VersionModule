﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VersionModule.Models
{
    public class VersionItem : Development_Version
    {
        public clsOntologyItem RefItem { get; set; }
        public long OrderId { get; set; }
        public clsOntologyItem Direction { get; set; }
        public clsOntologyItem RelationType { get; set; }

    }
}
